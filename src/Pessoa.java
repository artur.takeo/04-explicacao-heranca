import java.time.LocalDate;

abstract public class Pessoa {
	protected String nome;
	protected String cpf;
	public LocalDate dataNascimento;
	public Endereco endereco;
	
	public Pessoa(String nome, String cpf) {
		this.nome = nome;
		this.cpf = cpf;
	}
	
	public String getNome() {
		return nome;
	}
	
	public String getCpf() {
		return cpf;
	}
	
	public String toString() {
		return nome + " - " + cpf;
	}
	
	public abstract String getId();
}
